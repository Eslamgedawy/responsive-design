$(document).ready(function() {
    'use strict';
    $(".list-info li").click(function() {
      $(this).addClass('selected').siblings('li').removeClass('selected');
      $(".content-info div").hide();
      $( '.' + $(this).data('class')).fadeIn();
    });
  });